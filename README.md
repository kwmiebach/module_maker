## Make a python module — the automated way

This is a forked and modified version of https://github.com/LINKIT-Group/pyshipper

The project was renamed from pyshipper to module_maker. AWS and Uploading to Pypi
was removed.

See their blog article:
[ITNEXT: Create, build and ship a Python3 pip module in 5 minutes](https://itnext.io/create-build-and-ship-a-python3-pip-module-in-5-minutes-31dd6d9d5c8f?source=friends_link&sk=311381bad115c64f022902e62aa582ff)

### Prerequisites
* Docker.
* Build tools: apt install build-essential

### Module maker
Module maker helps setting up a module structure, adds some boilerplate code,
and provides a pipeline to build, test and ship a module, saving time when
you update or create a module.

The relevant code is mostly in Makefile, complemented with some Docker configuration and a
slightly modified setup.py. For the curious readers, this previous
[Docker & Makefile](https://itnext.io/docker-makefile-x-ops-sharing-infra-as-code-parts-ea6fa0d22946?source=friends_link&sk=1c42525c25039efadcbd25776a3019dd)
article explains the base. Module maker is a version that does the job of Python module delivery.

### Steps
 1. Set the module name
 2. Create a new module directory
 3. Configure variables
 4. Run and edit code
 5. Make module
 6. Publish

I will go through each step, and explain where and how Module maker kicks in to automate a
few things along the way.

### 1. Set the module name

    # we are using the module name as directory name
    MODULE_DIR=../example_module

### 2. Create a new module structure
This
[documentation](https://python-packaging.readthedocs.io/en/latest/minimal.html)
on Python packages describes the required package structure. Even as we automated
this step, the documentation is still essential and useful for debugging.

Let’s go to the one-liners.

    # get a copy of Module maker and change into it
    git clone https://gitlab.com/kwmiebach/module_maker.git
    cd module_maker

    # Continue with 3. now if you develop module_maker.

    # Now fork (clone) the contents to a new directory:
    make fork dest=${MODULE_DIR}

    # change to the new directory
    cd ${MODULE_DIR}


This forks a stripped version of Module maker to a new directory ${MODULE_DIR} -
files like LICENSE and README.md are not copied. After all, you own the new
module, and thus need to add a license and documentation to it — no license
strings attached.

### 3. Configure variables
Our next step is to edit the ./variables file.

    # edit the ./variables file
    vim variables

The NAME variable in ./variables is picked up by Makefile, and all variables are
exported to the environment of a Docker container,  used by setup.py during
container execution.

### 4. Run and edit code
Module maker ships with a ./module directory containing boilerplate code for a
module. There is also a coding pattern, with a minimal “hello-world”-like
function, that can be called both CLI- and import-style.

    # enter the container runtime
    # if you do this repeatedly, remember to remove the latest image first.
    make shell

    # test run the module in CLI
    python3 -m module

    # to test argument parsing:
    python3 -m module --name="test123"

This runs the code under ./module. Instead of using the name “module”, you can also
replace it with the name of your own module. A symlink is created to make both
work — in the shipped version, only your own name can be used to reference the module.

Now is a good time to modify or replace the code in ./module and start iterating. 
Remember to remove the latest image before the next "make shell"

### 5. Make module
When you have a working version it is time to build the package.
First update the VERSION in ./variables and then build the module:

    # this runs setup.py in the container
    make module

    # or if you want to include pylint:
    # make pylint module

The output of the build process is a gzipped tar archive, present in the
./dist directory. You can install it on any capable system and use it
like any other Python3 pip module.


### 6. Publish
The module can be published by uploading the archive file to the Python
Package Index [PyPi](https://pypi.org/).

The functionality to upload the archive was removed from module_maker.
You could upload it manually or use [pyshipper](https://github.com/LINKIT-Group/pyshipper).
