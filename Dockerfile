FROM alpine as base

RUN apk update
RUN apk add --no-cache \
        bash \
        python3 \
        build-base \
        python3-dev

RUN apk add --no-cache \
        openssh

RUN apk add --no-cache \
        ca-certificates

RUN apk add --no-cache \
        groff \
        git \
        zip \
        git-subtree \
        jq \
        unzip \
        busybox-extras

RUN pip3 install --upgrade pip


RUN python3 -m pip install pylint
RUN python3 -m pip install boto3
RUN python3 -m pip install jinja2

# Twine is a utility for publishing Python packages on PyPI:
# RUN python3 -m pip install twine

# Provides a unified command line interface to AWS:
# RUN python3 -m pip install awscli

RUN rm -rf /opt/build/*
RUN rm -rf /var/cache/apk/*
RUN rm -rf /root/.cache/*
RUN rm -rf /tmp/*

FROM scratch as user
COPY --from=base . .

ARG HOST_UID=${HOST_UID:-4000}
ARG HOST_USER=${HOST_USER:-nodummy}

RUN [ "${HOST_USER}" == "root" ] || \
    (adduser -h /home/${HOST_USER} -D -u ${HOST_UID} ${HOST_USER} \
    && chown -R "${HOST_UID}:${HOST_UID}" /home/${HOST_USER})

USER ${HOST_USER}
WORKDIR /home/${HOST_USER}
COPY files/profile .profile
