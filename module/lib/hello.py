"""
THIS SPECIFIC FILE IS DISTRIBUTED UNDER THE UNLICENSE: http://unlicense.org.

THIS MEANS YOU CAN USE THIS CODE EXAMPLE TO KICKSTART A PROJECT YOUR OWN.
AFTER YOU CREATED YOUR OWN ORIGINAL WORK, YOU CAN REPLACE THIS HEADER :)
"""

def write_message(default):

    """
    Parse any additional arguments
    """
    import sys
    if sys.argv:
        # called through CLI
        module_name = __loader__.name.split('.')[0]
        import argparse
        parser = argparse.ArgumentParser(
            prog=module_name,
            description="This is my new shiny pip package called: " + module_name,
        )

        parser.add_argument('--name', action='store', nargs=1, required=False, type=str,
                            default=[default],
                            help="Add a name")

        import sys
        args = parser.parse_args(sys.argv[1:])

        if args.name:
            # argparser provides us a list, even if only one argument
            if isinstance(args.name, list) and isinstance(args.name[0], str):
                name = args.name[0]


    message = str("Hello %s !" % name)
    import sys
    sys.stdout.write(message + "\n")
    return 0

