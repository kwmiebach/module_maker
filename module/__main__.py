"""
THIS SPECIFIC FILE IS DISTRIBUTED UNDER THE UNLICENSE: http://unlicense.org.

THIS MEANS YOU CAN USE THIS CODE EXAMPLE TO KICKSTART A PROJECT YOUR OWN.
AFTER YOU CREATED YOUR OWN ORIGINAL WORK, YOU CAN REPLACE THIS HEADER :)
"""

# This file is called when run as python3 -m ${MODULE}
# or when python is called on the zip file as python3 ${MODULE}.zip

if __name__ == '__main__':

    from .lib.hello import write_message
    result = write_message(default="world")

    import sys
    sys.exit(result)
